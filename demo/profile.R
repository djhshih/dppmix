library(devtools)
library(lineprof)

load_all("../R")

# Simulate data ##############################################################

set.seed(1)

ns <- c(20, 50, 30);
means <- list(c(-6, 0), c(0, -10), c(0, 6));

d <- rmvnorm_clusters(ns, means);
X <- d$X;
cl <- d$cl;


# Fit model ##################################################################

prof <- lineprof(
  {
    mcmc <- dppmix_mvnorm(X, verbose=FALSE);
  }
);

# Show results ###############################################################

p1 <- focus(prof, filename="dppmix_mvnorm.R")
shine(p1)

